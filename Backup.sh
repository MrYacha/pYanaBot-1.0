#!/bin/bash
#Postgresql backup script!
#For YanaBot, by MrYacha
DB_NAME=y1
FPATH="/home/yacha/Backups" #Path to backup folder
mkdir -p $FPATH/$DB_NAME
DATE=$(date +%d-%m-%YI%H-%M-%S) #out: 21-06-2018|20-07-00
FFILE=$FPATH/$DB_NAME/$DATE
echo Working...
pg_dump $DB_NAME > $FFILE.db
echo Done, you backup - $FFILE

#Log Section

touch $FPATH/$DB_NAME.log
echo $DATE: backup - $FFILE >> $FPATH/$DB_NAME.log
